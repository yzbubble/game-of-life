; $(document).ready(function () {
    new GameOfLifeAdapter().start();
});

function GameOfLifeAdapter(newSattus) {
    var s = $.extend(true, {
        isEditMode: false,
        selectors: {
            gameContainer: "#game"
        },
        imports: {
            game: new GameOfLife(),
            util: new Util()
        }
    }, newSattus);

    return {
        start: start
    };

    function start() {
        s.imports.game.repaintGrid().seed("random");
        centerContainer();
        regist();
    }

    function centerContainer() {
        var $game = $(s.selectors.gameContainer);
        if ($game.height() < window.innerHeight) {
            var marginTop = (window.innerHeight - $game.height()) / 2;
            $game.css({
                marginTop: marginTop
            });
        }
    }

    function regist() {
        $("#start").click(function () {
            if ($(this).text().trim() === "开始") {
                $(this).text("终止")
                s.imports.game.start();
            } else {
                $(this).text("开始")
                s.imports.game.stop();
            }
        });

        $("#nextFrame").click(function () {
            s.imports.game.nextFrame();
        });

        $("#randomFlushAliveCount").click(function () {
            s.imports.game.seed("random", {
                aliveCount: $("#aliveCount").val()
            }).renderStatus();
        });

        $("#edit").click(function () {
            var $this = $(this);
            if (s.isEditMode) {
                s.isEditMode = false;
                $this.text("保存成功").attr("disabled", "disabled");
                setTimeout(function () {
                    $this.text("进入编辑状态").removeAttr("disabled");
                }, 500);
            } else {
                s.isEditMode = true;
                $this.text("退出编辑状态");
            }
        });

        $("#allDead").click(function () {
            s.imports.game.seed("allDead").renderStatus();
        });

        $("#allAlive").click(function () {
            s.imports.game.seed("allAlive").renderStatus();
        });

        $("#settingTrigger").click(function () {
            var $this = $(this);
            var $panel = $("#settingPanel");
            if ($this.hasClass("close")) {
                $this.removeClass("close");
                $panel.removeClass("close");
            } else {
                $this.addClass("close");
                $panel.addClass("close");
            }
        });

        $("#repaintGrid").click(function () {
            s.imports.game.grid.setStatus({
                row: parseFloat($("#row").val())
            });
            s.imports.game.grid.row.setStatus({
                column: parseFloat($("#column").val())
            });
            s.imports.game.grid.row.cell.setStatus({
                style: {
                    width: parseFloat($("#cellWidth").val()),
                    height: parseFloat($("#cellHeight").val())
                }
            });
            s.imports.game.repaintGrid().seed("random");
            centerContainer();
            gameInnerDomEventRegist();
        });

        $("#saveFps").click(function () {
            var $this = $(this);
            var fps = parseInt($("#fps").val());
            var interval = typeof fps === "number" && ~~(1000 / fps);
            s.imports.game.setStatus({
                interval: interval
            });
            $this.text("保存成功").attr("disabled", "disabled");
            setTimeout(function () {
                $this.text("保存").removeAttr("disabled");
            }, 500);
        });

        gameInnerDomEventRegist();

        function gameInnerDomEventRegist() {
            $(s.imports.util.formatString(".{0}", s.imports.game.grid.row.cell.getStatus().classNameList.join("."))).click(function () {
                var $this = $(this);
                if (s.isEditMode) {
                    s.imports.game.toggleLifeStatus($this).renderStatus();
                }
            });
        }
    }
}

function GameOfLife(newStatus) {
    var s = {
        imports: {
            util: new Util(),
            grid: new Grid(),
        },
        row: null,
        interval: null,
        wantStop: false,
        gridStatus: {},
        selectors: {
            gameContainer: "#game"
        },
        enums: {
            lifeStatus: {
                dead: "dead",
                alive: "alive",
            },
            seedMode: {
                random: "random",
                allDead: "allDead",
                allAlive: "allAlive",
            }
        },
        consts: {
            currentStatusDataAttrName: "current-status",
            nextStatusDataAttrName: "next-status",
        },
        defaults: {
            row: 10,
            interval: 50
        }
    };

    setStatus(newStatus);

    return {
        grid: s.imports.grid,
        start: start,
        stop: stop,
        nextFrame: nextFrame,
        seed: seed,
        repaintGrid: repaintGrid,
        renderStatus: renderStatus,
        setStatus: setStatus,
        setDead: setDead,
        setAlive: setAlive,
        toggleLifeStatus: toggleLifeStatus,
        isDead: isDead,
        isAlive: isAlive
    };

    function setStatus(newStatus) {
        newStatus && $.extend(true, s, newStatus);
        for (var propertyName in s.defaults) {
            if (s.hasOwnProperty(propertyName) && s.imports.util.isFalseButNotBoolFalseAndIntZero(s[propertyName])) {
                s[propertyName] = s.defaults[propertyName];
            }
        }
        return this;
    }

    function repaintGrid() {
        $(s.selectors.gameContainer).html("").append(s.imports.grid.create());
        return this;
    }

    function renderStatus() {
        $(s.imports.util.formatString(".{0}", s.imports.grid.row.cell.getStatus().classNameList.join("."))).toArray().forEach(function (cell, index, array) {
            var $cell = $(cell);
            var cellNextStatus = $cell.data(s.consts.nextStatusDataAttrName);
            if (cellNextStatus === s.enums.lifeStatus.alive) {
                $cell.data(s.consts.currentStatusDataAttrName, s.enums.lifeStatus.alive);
                renderAlive($cell);
            } else if (cellNextStatus === s.enums.lifeStatus.dead) {
                $cell.data(s.consts.currentStatusDataAttrName, s.enums.lifeStatus.dead);
                renderDead($cell);
            }
        });
        return this;
    }

    function seed(mode, option) {
        option = option || {};
        var seedModeEnums = s.enums.seedMode;
        switch (mode) {
            case seedModeEnums.allAlive:
                var coordinate, $cell;
                var cellCount = s.imports.grid.row.getStatus().column * s.imports.grid.getStatus().row;
                for (i = 0; i < cellCount; i++) {
                    coordinate = convertToCoordinate(i);
                    $cell = getCell(coordinate.rowIndex, coordinate.columnIndex);
                    setAlive($cell);
                }
                break;
            case seedModeEnums.allDead:
                var cellCount = s.imports.grid.row.getStatus().column * s.imports.grid.getStatus().row;
                for (i = 0; i < cellCount; i++) {
                    coordinate = convertToCoordinate(i);
                    $cell = getCell(coordinate.rowIndex, coordinate.columnIndex);
                    setDead($cell);
                }
                break;
            case seedModeEnums.random:
                var random, $cell, aliveCount = 0, cellCount = s.imports.grid.getStatus().row * s.imports.grid.row.getStatus().column;
                var maxAliveCount = ~~(cellCount * 0.5);
                var percentReg = /^(\d+(?:\.\d+)?)%/, percent;
                if (typeof option.aliveCount === "string" && percentReg.test(option.aliveCount)) {
                    percent = parseFloat(percentReg.exec(option.aliveCount)[1]);
                    if (percent >= 0 && percent <= 100) {
                        maxAliveCount = ~~(cellCount * percent / 100);
                    }
                } else if (!isNaN(parseFloat(option.aliveCount))) {
                    if (option.aliveCount > 0 && option.aliveCount < 1) {
                        maxAliveCount = ~~(cellCount * option.aliveCount);
                    } else {
                        maxAliveCount = option.aliveCount;
                    }
                }
                var array = $(s.imports.util.formatString(".{0}", s.imports.grid.row.cell.getStatus().classNameList.join("."))).toArray();
                seed(seedModeEnums.allDead);
                while (true) {
                    random = s.imports.util.getRandom(0, array.length);
                    $cell = $(array[random]);
                    setAlive($cell);
                    array.splice(random, 1);
                    aliveCount++;
                    if (aliveCount >= maxAliveCount || aliveCount >= cellCount) {
                        break;
                    }
                }
                break;
            default:
                seed(seedModeEnums.random);
                break;
        }
        renderStatus();
        return this;
    }

    function start() {
        s.wantStop = false;
        traverse();
        return this;
    }

    function stop() {
        s.wantStop = true;
        return this;
    }

    function nextFrame() {
        var i,
            cellCount = s.imports.grid.getStatus().row * s.imports.grid.row.getStatus().column;

        for (var i = 0; i < cellCount; i++) {
            executeGameRules(getSurroundingCells(i));
        }
        renderStatus();

        return this;
    }

    function traverse() {
        nextFrame();
        if (!s.wantStop) {
            setTimeout(function () {
                traverse();
            }, s.interval);
        }
    }

    function isDead(cell) {
        var $cell = $(cell);
        return $cell.data(s.consts.currentStatusDataAttrName) === s.enums.lifeStatus.dead;
    }

    function isAlive(cell) {
        var $cell = $(cell);
        return $cell.data(s.consts.currentStatusDataAttrName) === s.enums.lifeStatus.alive;
    }

    function setDead(cell) {
        var $cell = $(cell);
        $cell.data(s.consts.nextStatusDataAttrName, s.enums.lifeStatus.dead);
        return this;
    }

    function setAlive(cell) {
        var $cell = $(cell);
        $cell.data(s.consts.nextStatusDataAttrName, s.enums.lifeStatus.alive);
        return this;
    }

    function toggleLifeStatus(cell) {
        var $cell = $(cell);
        if (isAlive($cell)) {
            setDead($cell);
        } else {
            setAlive($cell);
        }
        return this;
    }

    function renderDead(cell) {
        var $cell = $(cell);
        $cell.css("backgroundColor", "white");
        return this;
    }

    function renderAlive(cell) {
        var $cell = $(cell);
        $cell.css("backgroundColor", "black");
        return this;
    }

    function getCell(rowIndex, columnIndex) {
        var selector = s.imports.util.formatString(".{gridSelector} .{rowSelector}:eq({rowIndex}) .{cellSelector}:eq({columnIndex})", {
            gridSelector: s.imports.grid.getStatus().classNameList.join("."),
            rowSelector: s.imports.grid.row.getStatus().classNameList.join("."),
            cellSelector: s.imports.grid.row.cell.getStatus().classNameList.join("."),
            rowIndex: rowIndex,
            columnIndex: columnIndex
        });
        return $(selector);
        // return $(".gameoflife_grid .gameoflife_row:eq(" + rowIndex + ") .gameoflife_cell:eq(" + columnIndex + ")");
    }

    function getSurroundingCells(index) {
        var index = typeof index === "number" ? index : s.currentTraverseIndex;
        var coordinate = convertToCoordinate(index);
        var surroundingCells = [], x, y, realX, realY;
        for (y = coordinate.rowIndex - 1; y <= coordinate.rowIndex + 1; y++) {
            for (x = coordinate.columnIndex - 1; x <= coordinate.columnIndex + 1; x++) {
                if (y >= 0 && y < s.imports.grid.getStatus().row && x >= 0 && x < s.imports.grid.row.getStatus().column) {
                    surroundingCells.push(getCell(x, y));
                } else {
                    surroundingCells.push(undefined);
                }
            }
        }
        return surroundingCells;
    }

    function executeGameRules(nineCells) {
        if (!nineCells || nineCells.length !== 9) {
            return;
        }
        var centerCell = nineCells[4];
        var aliveCells = $.grep(nineCells, function (cell, index) {
            return cell && cell !== centerCell && isAlive(cell);
        });
        if (aliveCells.length === 3) {
            setAlive(centerCell);
        } else if (aliveCells.length === 2) {
            // nothing
        } else {
            setDead(centerCell);
        }
    }

    function convertToCoordinate(cnt) {
        var column = s.imports.grid.row.getStatus().column;
        return {
            rowIndex: ~~(cnt / column),
            columnIndex: cnt % column
        };
    }
}

function Util() {
    return {
        getRandom: getRandom,
        formatString: formatString,
        isNullOrUndefinedOrEmpty: isNullOrUndefinedOrEmpty,
        isNullOrUndefinedOrWhiteSpace: isNullOrUndefinedOrWhiteSpace,
        isFalseButNotBoolFalseAndIntZero: isFalseButNotBoolFalseAndIntZero
    }

    function getRandom(min, max) {
        var random = min + Math.round(Math.random() * (max - min));
        return random;
    }

    function formatString(input, args) {
        var result = input;
        if (arguments.length > 1) {
            if (typeof (args) === "object") {
                for (var key in args) {
                    if (args[key] !== undefined) {
                        var reg = new RegExp("({" + key + "})", "g");
                        result = result.replace(reg, args[key]);
                    }
                }
            } else {
                for (var i = 1; i < arguments.length; i++) {
                    if (arguments[i] !== undefined) {
                        var reg = new RegExp("({)" + (i - 1) + "(})", "g");
                        result = result.replace(reg, arguments[i]);
                    }
                }
            }
        }
        return result;
    }

    function isNullOrUndefinedOrWhiteSpace(input) {
        return input === undefined || input === null || /^\s*$/.test(input);
    }

    function isNullOrUndefinedOrEmpty(input) {
        return input === undefined || input === null || input === "";
    }

    function isFalseButNotBoolFalseAndIntZero(input) {
        return !input && input !== false && input !== 0;
    }
}

function Grid(newStatus) {
    var s = {
        row: null,
        template: defaultTemplate,
        classNameList: ["gameoflife_grid"],
        style: {
            overflow: "auto",
            display: "inline-block"
        },
        imports: {
            util: new Util(),
            row: new Row()
        },
        defaults: {
            row: 10
        }
    };

    setStatus(newStatus);

    return {
        row: s.imports.row,
        create: create,
        getStatus: getStatus,
        setStatus: setStatus,
    };

    function getStatus() {
        return s;
    }

    function setStatus(newStatus) {
        newStatus && $.extend(true, s, newStatus);
        for (var propertyName in s.defaults) {
            if (s.hasOwnProperty(propertyName) && s.imports.util.isFalseButNotBoolFalseAndIntZero(s[propertyName])) {
                s[propertyName] = s[propertyName] || s.defaults[propertyName];
            }
        }
        return this;
    }

    function create(row, column) {
        row = typeof row === "number" ? row : s.row;
        var template = typeof (s.template) === "function" ? s.template() : s.template;
        var $grid = $(template).css(s.style);
        for (var i = 0; i < s.row; i++) {
            s.imports.row.create(column).appendTo($grid);
        }
        return $grid;
    }

    function defaultTemplate() {
        return "<div class='" + s.classNameList.join(" ") + "'></div>";
    }
}

function Row(newStatus) {
    var s = {
        column: null,
        style: {
            fontSize: "0px",
            lineHeight: "0px"
        },
        template: defaultTemplate,
        classNameList: ["gameoflife_row"],
        imports: {
            util: new Util(),
            cell: new Cell()
        },
        defaults: {
            column: 10
        }
    };

    setStatus(newStatus);

    return {
        cell: s.imports.cell,
        create: create,
        getStatus: getStatus,
        setStatus: setStatus,
    };

    function getStatus() {
        return s;
    }

    function setStatus(newStatus) {
        newStatus && $.extend(true, s, newStatus);
        for (var propertyName in s.defaults) {
            if (s.hasOwnProperty(propertyName) && s.imports.util.isFalseButNotBoolFalseAndIntZero(s[propertyName])) {
                s[propertyName] = s[propertyName] || s.defaults[propertyName];
            }
        }
        return this;
    }

    function create(column) {
        column = typeof column === "number" ? column : s.column;
        var template = typeof (s.template) === "function" ? s.template() : s.template;
        var $row = $(template).css(s.style);
        for (var i = 0; i < s.column; i++) {
            s.imports.cell.create().appendTo($row);
        }
        return $row;
    }

    function defaultTemplate() {
        return "<div class='" + s.classNameList.join(" ") + "'></div>";
    }
}

function Cell(newStatus) {
    var s = {
        style: {
            width: null,
            height: null,
            display: "inline-block",
            border: "1px solid black"
        },
        template: defaultTemplate,
        classNameList: ["gameoflife_cell"],
        defaults: {
            style: {
                width: 20,
                height: 20
            }
        },
        imports: {
            util: new Util()
        }
    };

    setStatus(newStatus);

    return {
        create: create,
        setStatus: setStatus,
        getStatus: getStatus,
    };

    function getStatus() {
        return s;
    }

    function setStatus(newStatus) {
        newStatus && $.extend(true, s, newStatus);

        (function setDefaultValueIfInvalid(src, defaults) {
            for (var propertyName in defaults) {
                if (src.hasOwnProperty(propertyName)) {
                    if (typeof defaults[propertyName] === "object") {
                        setDefaultValueIfInvalid(src[propertyName], defaults[propertyName]);
                    } else {
                        src[propertyName] = !s.imports.util.isFalseButNotBoolFalseAndIntZero(src[propertyName]) ? src[propertyName] : defaults[propertyName];
                    }
                }
            }
        })(s, s.defaults);

        return this;
    }

    function create() {
        var template = typeof (s.template) === "function" ? s.template() : s.template;
        var $cell = $(template).css(s.style);
        return $cell;
    }

    function defaultTemplate() {
        return "<div class='" + s.classNameList.join(" ") + "'></div>";
    }
}